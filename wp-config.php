<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N.L`F;)#tNR]_m@*!y(%8,5L.$j3LA=.jl g)61UBKD7[r:$&-s~EJH+X*v&H7Oz');
define('SECURE_AUTH_KEY',  ';qQlNyk|*3DI08X4r}W@xWq/}:9k4p1RgwVz/sM)aCI3eD8Xwysc0#$TP{1bI+<`');
define('LOGGED_IN_KEY',    'n6<s}>Y<xAge(5Nm4c$bW1c}7]x^Se: AE`(;0(zX`:B8XfC_xpxk_4YQ3mvMkXg');
define('NONCE_KEY',        'd|`4-g*nH 3&a;]GE`<th&_y,e$gW?vb![y@:^|yK|*mMaKR#;j[*b:oo5DW6ae^');
define('AUTH_SALT',        '5HMLxt^ml|9pp)f-=X=B:J`/U1s-!+e=e})z#t0DZ^GGd6qlV!-MLR/(qaL2wV.^');
define('SECURE_AUTH_SALT', '(i3Vf)]0Ez@ec>;vTQca#z.P}[[d<~Cl_YaeEi$OcE!PwD_2!r_wZOfXej;3zkF`');
define('LOGGED_IN_SALT',   'mUI&HSDGm~d?Id/Wd(q7>6zMf5I-zstOR7aW[lAx{3gd0T75Y,|0))[x_m6EnbOo');
define('NONCE_SALT',       'm.}+iO6/nPsu!&_/weS_T6&y:Q?h5#Z aChdEZ}&UvB`Nh^q 6V38CRJY;1!`(sg');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
